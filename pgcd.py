"""Le PGCD de deux nombres entiers a et b est le plus grand nombre
    appartenant à l'ensemble D de tous les diviseurs communs de a et b.
    Ce script permet de déterminer le PGCD de 2 nombres entiers"""

import doctest
import argparse
import sys
import logging


def pgcd(a, b):
    """Renvoi le PGCD des nombres entiers a et b
    Amélioration modèle tuto: L'argument -v de doctest collisionne avec
    l'argument -v du module pgcd donc il faudrait le préciser dans le tuto"

    >>> pgcd(561, 357)
    51
    >>> pgcd(48, 32)
    16
    >>> pgcd(5, 4)
    1
    """

    x = a - b
    if args.debug:
        logging.debug(f'{a} - {b} = {x}')

    while x != 0:
        a = b
        b = x
        if a < b:
            b = a
            a = x
        x = a - b
        if args.debug:
            logging.debug(f'{a} - {b} = {x}')
    return b

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Détermine le PGCD de 2 nombres entiers.')
    parser.add_argument('entier', metavar='entier', type=int, nargs=2, help=__doc__)

    parser.add_argument('-v',
                        '--verbose',
                        dest='verbose',
                        action='store_true'
                        )

    parser.add_argument('-d',
                        '--debug',
                        dest='debug',
                        action='store_true'
                        )

    parser.add_argument('-l',
                        '--logfile',
                        dest='logfile',
                        action='store_true'
                        )

    args = parser.parse_args()

    if args.logfile:
        logging.basicConfig(filename='logfile.log', level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.DEBUG)

    if args.entier[0] <= args.entier[1]:
        if args.debug:
            logging.critical(f'{args.entier[0]}<{args.entier[1]} mais a doit être plus grand que b')
        else:
            print('a doit être plus grand que b')
        sys.exit()

    if args.verbose:
        print(f'PGCD({args.entier[0]}; {args.entier[1]}) = {pgcd(args.entier[0], args.entier[1])}')
    else:
        print(pgcd(args.entier[0], args.entier[1]))
        doctest.testmod()

    if args.debug:
        logging.info(f'Voici le pgcd: {pgcd(args.entier[0], args.entier[1])}')
